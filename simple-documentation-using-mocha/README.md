# ToDo-User-API-Tests-Mocha-Chai
A Simple ToDo list example with User Login/Signup with Test cases written in Mocha-Chat testing framework.
component :
1. Node JS
2. Express
3. Mongoose
4. JWT
5. Mocha-Chat (Documentation)

### Steps

Run the app
```
npm start
```

Run test

```
npm test
```

Create Documentation
```
apidoc -i apidoc/ -o apidoc/doc/
```

Read Documentation on folder ../apidoc/doc/index.html
