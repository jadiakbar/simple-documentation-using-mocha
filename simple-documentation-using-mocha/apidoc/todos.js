/**
 * @api {get} /todos To Do
 * @apiName To Do Create List
 * @apiGroup Todos
 * 
 * @apiHeader x-auth Authorization Basic Access Authentication token.
 *
 * @apiParamExample Success-Response:
 *     HTTP/1.1 200 OK
        {
            "todos": [
                {
                    "completed": false,
                    "completedAt": null,
                    "_id": "618059af0ed00833a07111ee",
                    "text": "First test todo",
                    "_creator": "618059af0ed00833a07111ec",
                    "__v": 0
                }
            ]
        }

 *
 * @apiError Unauthorized The Authorized not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Unauthorized"
 *     }
 */