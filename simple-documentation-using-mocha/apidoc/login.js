/**
 * @api {post} /users/login Login user
 * @apiName Login user
 * @apiGroup Login
 *
 * @apiParam {String} email Email for Login.
 * @apiParam {String} password Password for Login, minimum 8 character.
 *
 * @apiParamExample Success-Response:
 *     HTTP/1.1 200 OK
 *     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MTgwNTlhZjBlZDAwODMzYTA3MTExZWMiLCJhY2Nlc3MiOiJhdXRoIiwiaWF0IjoxNjM1ODA1NTQ3fQ.5WMmUiOlTW4B9crk1wvs4AKZF2LwHMCqSnqGOf0V3PI"
 *
 *
 * @apiError Unauthorized The Authorized not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": "Unauthorized"
 *     }
 */