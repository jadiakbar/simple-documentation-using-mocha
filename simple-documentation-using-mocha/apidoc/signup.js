/**
 * @api {post} /users Signup new user
 * @apiName Signup new user
 * @apiGroup Signup
 * 
 * @apiHeader x-auth Authorization Basic Access Authentication token.
 *
 * @apiParam {String} email Email for signup.
 * @apiParam {String} password Password for signup, minimum 8 character.
 *
 * @apiParamExample Success-Response:
 *     HTTP/1.1 200 OK
        {
            "_id": "61803ff980bfcb4924b47e90",
            "email": "jadiakbar@gmail.com"
        }

 *
 * @apiError Duplicate Authorized found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
            "driver": true,
            "name": "MongoError",
            "index": 0,
            "code": 11000,
            "keyPattern": {
                "email": 1
            },
            "keyValue": {
                "email": "balala@gmail.com"
            }
        }
 */